package capaGrafica;

import javax.swing.JComboBox;
import javax.swing.JTextField;

public class Cleaner {

	public static void limpiarJText(JTextField texto){
		texto.setText("");
	}
	
	public static void reestablecerComboBox(JComboBox<?> comboBox){
		if(comboBox.getItemAt(0) != null)
			comboBox.setSelectedIndex(0);
		else
			throw new RuntimeException("No se pudo restablecer ComboBox "
					+ "ya que no tiene parametros cargados");
	}
}
