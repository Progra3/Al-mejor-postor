package capaGrafica;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTable;

public class VentanaOfertas {

	private JFrame frame;
	private JTable table;
	
	Object[][] datos;
	int filas = 5, columnas = 5;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VentanaOfertas window = new VentanaOfertas();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public VentanaOfertas() {
		frame = new JFrame();
		frame.setBounds(100, 100, 400, 500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		
		incializarTabla();
	}

	private void incializarTabla() {
		table = new JTable(getDatos(), getNombreColumnas());
		table.setBounds(10, 247, 414, -235);
		frame.getContentPane().add(table);
		
//		JScrollPane scrollPane = new JScrollPane(table);
//		table.setFillsViewportHeight(true);
	}

	private Object[][] getDatos() {
		Object[][] ret = new Object[filas][columnas];
		ret[0][0] = "Lucas";
		return ret;
	}
	
	private Object[] getNombreColumnas(){
		return new Object[]{"Nombe", "Apellido", "Desde", "Hasta", "Oferta" };
		
	}
}
