package capaGrafica;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import almacenamiento.Archivo;
import capaLogica.Datos;
import capaLogica.Dia;
import capaLogica.Oferta;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;

public class MainForm extends JFrame {
	private static final long serialVersionUID = 1L;
	
	private VentanaAlquileres ventanaAlquiler = VentanaAlquileres.instancia();
	private VentanaTabla ventanaMejoresOfertas;
	private VentanaTabla ventanaOfertas;
	
	private ArrayList<Datos> _datos;
	private ArrayList<Datos> _mejoresDatos;
	Archivo archivos;
	
	private JPanel contentPane;
	private JTextField textApellido;
	private JTextField textNombre;
	private JTextField textOferta;
	private JTextField textFieldArchivo;
	
	private JComboBox<String> comboBoxDesde;
	private JComboBox<String> comboBoxHasta;
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainForm frame = new MainForm();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public MainForm() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 700, 500);
		setResizable(false);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		_datos = new ArrayList<Datos>();
		_mejoresDatos = new ArrayList<Datos>();
		archivos = new Archivo();
		
		
		
		inicializarLabels();
		inicializarJtexts();
		inicializarComboBox();
		inicializarJbuttons();		
	}

	private void inicializarComboBox() {
		comboHasta();
		comboDesde();
	}

	private void inicializarJbuttons() {
		botonOfertas();	
		botonRegistrar();	
		botonAlquilar();
		botonPlanificarDia();
		botonGuardar();
		botonCargar();
	}
	
	private void inicializarLabels() {
		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setBounds(92, 11, 125, 32);
		contentPane.add(lblNombre);
		
		JLabel lblApellido = new JLabel("Apellido:");
		lblApellido.setBounds(92, 49, 139, 32);
		contentPane.add(lblApellido);
		
		JLabel lblHorario = new JLabel("Horarios: ");
		lblHorario.setBounds(92, 92, 144, 32);
		contentPane.add(lblHorario);
		
		JLabel lblDesde = new JLabel("Desde: ");
		lblDesde.setBounds(194, 92, 125, 32);
		contentPane.add(lblDesde);
		
		JLabel lblHasta = new JLabel("Hasta: ");
		lblHasta.setBounds(194, 135, 125, 40);
		contentPane.add(lblHasta);
		
		JLabel lblOferta = new JLabel("Oferta: $");
		lblOferta.setBounds(92, 186, 125, 32);
		contentPane.add(lblOferta);	
		
	}

	private void inicializarJtexts() {
		jTextFieldNombre();	
		jTextFieldApellido();
		jTextFieldOferta();	
		JTextFieldArchivo();
	}

	private void jTextFieldOferta() {
		textOferta = new JTextField();
		textOferta.setBounds(346, 192, 132, 20);
		contentPane.add(textOferta);
		textOferta.setColumns(10);
	}

	private void jTextFieldApellido() {
		textApellido = new JTextField();
		textApellido.setBounds(346, 55, 132, 20);
		contentPane.add(textApellido);
		textApellido.setColumns(10);
	}

	private void jTextFieldNombre() {
		textNombre = new JTextField();
		textNombre.setBounds(346, 17, 132, 20);
		contentPane.add(textNombre);
		textNombre.setColumns(10);
	}

	private void JTextFieldArchivo() {
		textFieldArchivo = new JTextField();
		textFieldArchivo.setBounds(346, 430, 172, 20);
		contentPane.add(textFieldArchivo);
		textFieldArchivo.setColumns(10);
		textFieldArchivo.setText("Inserte nombre de archivo");	
	}

	private void comboHasta() {
		comboBoxHasta = new JComboBox<String>();
		comboBoxHasta.setBounds(346, 145, 132, 20);
		contentPane.add(comboBoxHasta);
		cargarCamposComboBox(comboBoxHasta);
	
	}
	
	private void comboDesde() {
		comboBoxDesde = new JComboBox<String>();
		comboBoxDesde.setBounds(346, 98, 133, 20);
		contentPane.add(comboBoxDesde);
		cargarCamposComboBox(comboBoxDesde);
	}

	private void botonRegistrar() {
		JButton botonRegistrar = new JButton("Registrar oferta");
		botonRegistrar.setBounds(346, 238, 132, 23);
		contentPane.add(botonRegistrar);
		botonRegistrar.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (verificarCampos()){
					try{
						cargarDatos();
						cleanAll();	
						Advertiser.advertencia("Cargado con exito");
					}
				catch(Exception e){
						Advertiser.advertencia(e.getMessage());
					}

				}
			}
		});
	}

	private void botonAlquilar() {
		JButton botonAlquilar = new JButton("Alquilar instrumentos");
		botonAlquilar.setBounds(147, 317, 172, 23);
		contentPane.add(botonAlquilar);
		botonAlquilar.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				ventanaAlquiler.setVisible(true);
			}			
		});
		
	}

	private void botonPlanificarDia() {
		JButton planificarDia = new JButton("Planificar Dia");
		planificarDia.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
					
				obtenerMejorDia();
				ventanaMejoresOfertas = new VentanaTabla(_mejoresDatos);
				ventanaMejoresOfertas.setVisible(true);
		
			}

		});
		planificarDia.setBounds(346, 374, 172, 23);
		contentPane.add(planificarDia);

	}

	private void botonGuardar() {
		JButton botonGuardar = new JButton("Guardar Mejor Oferta");
		botonGuardar.setBounds(147, 374, 172, 23);
		contentPane.add(botonGuardar);
		botonGuardar.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				try{
					archivos.guardarOfertas(_mejoresDatos);
					Advertiser.advertencia("Guardado con exito");
				}
				catch(Exception exc){
					Advertiser.advertencia(exc.getMessage());
				}
				
			}
			
		});
	}

	private void botonCargar() {
		JButton botonCarga = new JButton("Cargar Archivo");
		botonCarga.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(verificarCarga()){
					try {
						_datos = archivos.leerOfertas(textFieldArchivo.getText());
						for(Datos d : _datos)
							System.out.println(d);
					} catch (Exception e) {
						Advertiser.advertencia(e.getMessage());
					}
				}
				else
					Advertiser.advertencia("Error de campo, debe ser dd-mm-aaaa");
			}
		});
		botonCarga.setBounds(147, 429, 172, 23);
		contentPane.add(botonCarga);
	}

	private void botonOfertas() {
		JButton botonOferta = new JButton("Ofertas realizadas");
		botonOferta.setBounds(346, 317, 172, 23);
		contentPane.add(botonOferta);
		botonOferta.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				for (Datos e : _datos)
					System.out.println(e.toString());
				ventanaOfertas = new VentanaTabla(_datos);
				ventanaOfertas.setVisible(true);
			}
		});
	}

	
	private void cargarDatos() throws Exception{
		String nombre = textNombre.getText();
		String apellido = textApellido.getText();
		int horaInicio = comboBoxDesde.getSelectedIndex() - 1;
		int horaFin = comboBoxHasta.getSelectedIndex() - 1;
		int monto = Integer.parseInt(textOferta.getText());
		int bateria = ventanaAlquiler.valorBateria();
		int guitarra = ventanaAlquiler.valorGuitarra();
		int bajo = ventanaAlquiler.valorBajo();
		
		Datos oferta = new Datos.Constructor().nombre(nombre).apellido(apellido).
				horaInicio(horaInicio).horaFin(horaFin).
				monto(monto + bateria + guitarra + bajo).alquiler(guitarra, bajo, monto).build();
		
		_datos.add(oferta);
	}

	
	private void obtenerMejorDia() {
		_mejoresDatos.clear();	
		Dia dia = new Dia();
		for(Datos datos : _datos){
			dia.registrarOferta(datos.getNombre(), datos.getApellido()
					, datos.getHoraInicio(), datos.getHoraFin(),
					datos.getMonto(), datos.getAlquiler());
		}
		dia.administrarReserva();
		for (Oferta oferta : dia.getReserva())
			_mejoresDatos.add(oferta.empaquetar(oferta));
	}
	
	
	private void cleanAll() {
		Cleaner.limpiarJText(textOferta);
		Cleaner.limpiarJText(textNombre);
		Cleaner.limpiarJText(textApellido);
		Cleaner.reestablecerComboBox(comboBoxDesde);
		Cleaner.reestablecerComboBox(comboBoxHasta);
	}

	

	public boolean verificarCarga(){
		return matcher("\\d{2}\\-\\d{2}\\-\\d{4}", textFieldArchivo.getText());
	}
	
	private boolean verificarCampos() {
		boolean verificacion = true;
		if (!matcher("[A-Z]\\w[^_0-9]*", textApellido.getText())){
			Advertiser.advertencia("Campo nombre invalido: "
					+ textNombre.getText());
			verificacion = false;
		}
		
		if (!matcher("[A-Z]\\w[^_0-9]*", textNombre.getText())){
			Advertiser.advertencia("Campo apellido no valido: "
					+ textApellido.getText());
			verificacion = false;
		}
		
		if (matcher(",", textOferta.getText())){
			Advertiser.advertencia("No se admiten centavos,"
					+ " ingrese los datos nuevamente");
			verificacion = false;	
		}
		
		if (!matcher("\\$*\\d+", textOferta.getText())){
			Advertiser.advertencia("Campo oferta no valido: "
					+ textOferta.getText());
			verificacion = false;
		}
		
		if(comboBoxDesde.getSelectedIndex() == 0 ||
				comboBoxHasta.getSelectedIndex() == 0){
			Advertiser.advertencia("Seleccione los horarios"
					+ " correspondientes");
			verificacion = false;
		}
		
		return verificacion;
	}
	
	
	private boolean matcher(String patron, String texto){
		Pattern patString = Pattern.compile(patron);
		Matcher text = patString.matcher(texto);
		
		return text.find();	
	}
	

	private void cargarCamposComboBox(JComboBox<String> cBox){
		cBox.addItem("Horarios");
		for (int i = 0; i < 24; ++i)
			if (i < 10)
				cBox.addItem("0"+i+":00 hs");
			else
				cBox.addItem(i+":00 hs");
	}
}
