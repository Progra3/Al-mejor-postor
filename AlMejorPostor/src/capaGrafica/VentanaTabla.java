package capaGrafica;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import capaLogica.Datos;

public class VentanaTabla extends JDialog {

	private static final long serialVersionUID = 1L;
	
	private final JPanel contentPanel = new JPanel();
	private JTable table;
	
	private Object[][] _datos;
	
	public VentanaTabla(List <Datos> informacion) {
		setBounds(100, 100, 700, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		setResizable(false);
				
		crearMatriz(informacion);
		iniciarTabla();				
		crearScrollPanel();	
		botonCancelar();
	}

	private void iniciarTabla() {
		table = new JTable();
		table.setModel(new DefaultTableModel(_datos, getNombreColumnas()));
	}

	private void crearScrollPanel() {
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 670, 207);
		contentPanel.add(scrollPane);		
		scrollPane.setViewportView(table);		
	}

	private void botonCancelar() {
		
		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		getContentPane().add(buttonPane, BorderLayout.SOUTH);
		
		JButton cancelButton = new JButton("Cerrar");
		cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		cancelButton.setActionCommand("Cerrar");
		buttonPane.add(cancelButton);
	}
	
	private Object[] getNombreColumnas(){
		return new Object[]{"Nombe", "Apellido", "Desde", "Hasta",
				"Monto con instrumentos"};		
	}
	
	private void crearMatriz(List<Datos> datos) {
		_datos = new Object[datos.size()][getNombreColumnas().length];
		for (int i = 0; i < datos.size(); ++i)
			setFila(i, datos.get(i));
		for(int i = 0; i < datos.size(); ++i)
			for (int j = 0; j < 5; ++j)
				System.out.println(_datos[i][j].toString());
	}
	
	private void setFila(int fila, Datos informacion){		
		int numeroColumna = 0;
		_datos[fila][numeroColumna++] = informacion.getNombre();
		_datos[fila][numeroColumna++] = informacion.getApellido();
		_datos[fila][numeroColumna++] = informacion.getHoraInicio();
		_datos[fila][numeroColumna++] = informacion.getHoraFin();
		_datos[fila][numeroColumna++] = informacion.getMonto();
	}
}
