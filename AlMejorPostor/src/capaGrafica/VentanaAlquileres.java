package capaGrafica;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JCheckBox;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class VentanaAlquileres extends JDialog {

	private static final long serialVersionUID = 1L;
	
	private static VentanaAlquileres instancia;
	
	private final JPanel contentPanel;
	private JCheckBox chckbxGuitarra;
	private JCheckBox chckbxBajo;
	private JCheckBox chckbxBateria;
	private JLabel valorTotal;
	
	private int valorGuitarra = 1200;
	private int valorBateria = 1400;
	private int valorBajo = 900;
	private int alquilerTotal = 0;

	
	public static VentanaAlquileres instancia(){
		if (instancia == null)
			return new VentanaAlquileres();
		else
			return instancia;
	}
	
	private VentanaAlquileres() {
		setDefaultCloseOperation(HIDE_ON_CLOSE);
		setBounds(100, 100, 400, 300);
		setResizable(false);
		
		contentPanel = new JPanel();
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPanel);
		contentPanel.setLayout(null);

		inicializarLabels(); 
		inicializarCheckBox();
		botonAceptar();
			
	}

	private void inicializarCheckBox() {
		checkBoxBateria();
		checkBoxGuitarra();
		checkBoxBajo();

	}

	private void inicializarLabels(){
		
		JLabel labelGuitarra = new JLabel("Valor: $" + valorGuitarra);
		labelGuitarra.setBounds(217, 49, 212, 43);
		contentPanel.add(labelGuitarra);
		
		JLabel labelBateria = new JLabel("Valor: " + valorBateria);
		getContentPane().add(labelBateria, BorderLayout.WEST);
		labelBateria.setBounds(217, 6, 212, 43);
		
		JLabel labelBajo = new JLabel("Valor: $" + valorBajo);
		labelBajo.setBounds(217, 92, 212, 43);
		contentPanel.add(labelBajo);
		
		JLabel lblTotal = new JLabel("Total: $");
		lblTotal.setBounds(5, 135, 212, 43);
		contentPanel.add(lblTotal);
		
		valorTotal = new JLabel(""+alquilerTotal);
		valorTotal.setBounds(217, 135, 212, 43);
		valorTotal.setBackground(Color.WHITE);
		contentPanel.add(valorTotal);
	}
	
	private void checkBoxBajo() {
		chckbxBajo = new JCheckBox("Bajo");
		chckbxBajo.setBounds(5, 92, 212, 43);
		contentPanel.add(chckbxBajo);
		chckbxBajo.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				total();
				actualizarLabel();				
			}
		});
	}

	private void checkBoxGuitarra() {
		chckbxGuitarra = new JCheckBox("Guitarra");
		chckbxGuitarra.setBounds(5, 49, 212, 43);
		contentPanel.add(chckbxGuitarra);
		chckbxGuitarra.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				total();
				actualizarLabel();
			}		
		});
	}

	private void checkBoxBateria() {
		chckbxBateria = new JCheckBox("Bateria");
		chckbxBateria.setBounds(5, 6, 212, 43);
		contentPanel.add(chckbxBateria);
		chckbxBateria.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				total();
				actualizarLabel();
				
			}
		});
	}
		
	private void botonAceptar(){
		JButton aceptar = new JButton("Aceptar");
		aceptar.setBounds(169, 189, 89, 23);
		contentPanel.add(aceptar);
		aceptar.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
			}		
		});
	}

	public int valorGuitarra(){
		if(chckbxGuitarra.isSelected())
			return valorGuitarra;
		else 
			return 0;
	}
	
	public int valorBateria(){
		if(chckbxBateria.isSelected())
			return valorBateria;
		else
			return 0;
	}
	
	public int valorBajo(){
		if(chckbxBajo.isSelected())
			return valorBajo;
		else
			return 0;
	}
	
	private void actualizarLabel() {
		valorTotal.setText("" + alquilerTotal);			
	}
	
	private void total(){
		int ret = 0;
		
		if (chckbxBajo.isSelected()) ret+= valorBajo; 
		if (chckbxBateria.isSelected())  ret += valorBateria ;
		if (chckbxGuitarra.isSelected())  ret += valorGuitarra;

		alquilerTotal = ret;	
		}

}
