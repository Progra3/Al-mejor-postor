package almacenamiento;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Scanner;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import capaLogica.Datos;

public class Archivo implements Serializable{

	private static final long serialVersionUID = 1L;

	Gson gson;
	
	public Archivo(){
		gson = new Gson();
	}
	
	public ArrayList<Datos> leerOfertas(String archivo) throws Exception{
		//archivo -> dd-mm-aaaa
		
		String jsonString = "";
		FileInputStream fis = new FileInputStream("C:\\Users\\Usuario\\Desktop\\" + archivo + ".txt");
		Scanner scanner = new Scanner(fis);
	
		while(scanner.hasNextLine()){
			String line = scanner.nextLine();
			jsonString = jsonString + line;
		}		
		scanner.close();
		
		ArrayList<Datos> lista = gson.fromJson(jsonString, new TypeToken<ArrayList<Datos>>(){}.getType());
		
		return lista;
	}
	
	public void guardarOfertas(List<Datos> datos) throws Exception{

		String fileName = diaGuardado();
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String stringJson = gson.toJson(datos);
		String path = "C:\\Users\\Usuario\\Desktop\\" + fileName + ".txt";

		try{
			FileWriter writer = new FileWriter(path);
			writer.write(stringJson);
			writer.close();
		}
		catch(Exception e) { 
			System.out.println("Error al guardar el archivo: " + e.getMessage()); 
		}		
	}

	public String diaGuardado(){
		
		Calendar calendar = Calendar.getInstance();

		Integer a�o = calendar.get(Calendar.YEAR);
		Integer mes = calendar.get(Calendar.MONTH);		
		Integer diaMes = calendar.get(Calendar.DATE);		
		
		return diaMes.toString() + "-" + (mes++).toString() + "-" + a�o.toString();
	}

}
