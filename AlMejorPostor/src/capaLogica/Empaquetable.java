package capaLogica;

public interface Empaquetable<E, J> {
	public J empaquetar(E obj);
	public E desempaquetar(J obj);
}
