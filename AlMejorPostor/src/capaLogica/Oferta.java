package capaLogica;

import java.io.Serializable;

public class Oferta implements Comparable<Oferta>, Empaquetable<Oferta, Datos>, Serializable {

	private static final long serialVersionUID = 1012861581657787279L;
	
	private Alquiler _alquiler;
	private String _nombreOferente;
	private String _apellidoOferente;

	private int _horaInicio;
	private int _horaFin;
	private int _monto;

	public Oferta(String nombre, String apellido, int inicio, int fin, int precio) {

		boolean okInicio = inicio < 24 && inicio >= 0;
		boolean okFin = fin <= 24 && fin > 0;
		boolean okFranja = inicio < fin;
		boolean okPrecio = precio >= 0;

		boolean requerimientos = okInicio && okFin && okFranja && okPrecio;

		if (requerimientos) {
			_horaInicio = inicio;
			_horaFin = fin;
			_monto = precio;

			_alquiler = new Alquiler(0, 0, 0);
			_nombreOferente = nombre;
			_apellidoOferente = apellido;

		} else {
			throw new IllegalArgumentException("Los horarios inicio: " + inicio + " y fin" + fin
					+ ", o el monto ingresados: " + precio + " no son validos!");
		}

	}

	public void registrarAlquiler(Alquiler sala) {
		_alquiler = sala;
		actualizarPrecio();
	}

	public void actualizarPrecio() {
		_monto += _alquiler.getValor();
	}
	
	public void anular(){
		_monto = 0;
	}
	
	public int getMonto() {
		return _monto;
	}

	public String getOferente() {
		return _nombreOferente + " " + _apellidoOferente;
	}

	boolean superpuestaCon(Oferta oferta) {
		if (_horaFin <= oferta._horaInicio)
			return false;
		
		return true;
	}
	
	int pagoPorHora(){
		int ret = _monto/duracion();
		return ret;
	}
	
	int duracion(){
		return _horaFin - _horaInicio;
	}

	@Override
	public int compareTo(Oferta oferta) {

		if (oferta == null)
			throw new RuntimeException("Oferta inexistente");

		if (oferta.getClass() != this.getClass())
			throw new ClassCastException("No se esta comparando con una oferta");

		if (_horaInicio == 0 && ((Oferta) oferta)._horaInicio == 0)
			return 0;

		if (_horaInicio == 0)
			return -1;

		if (((Oferta) oferta)._horaInicio == 0)
			return 1;

		if (this._horaInicio > ((Oferta) oferta)._horaInicio)
			return 1;

		if (this._horaInicio < ((Oferta) oferta)._horaInicio)
			return -1;

		else
			return 0;
	}

	public boolean equals(Object obj){
		if (obj == this)
			return true;
		if(this.getClass() != obj.getClass() || obj == null)
			return false;
		return _nombreOferente.equals(((Oferta)obj)._nombreOferente) &&
				_apellidoOferente.equals(((Oferta)obj)._apellidoOferente)&&
				_horaInicio ==((Oferta)obj)._horaInicio &&
				_horaFin ==((Oferta)obj)._horaFin &&
				_monto ==((Oferta)obj)._monto;
	}
	
	@Override
	public String toString() {

		return getOferente() + ": " + "$" + _monto + ", De " + _horaInicio + " a " + _horaFin + ".";
	}

	@Override
	public Oferta desempaquetar(Datos obj) {
		Oferta ret = new Oferta(obj.getNombre(), obj.getApellido()
					, obj.getHoraInicio(), obj.getHoraFin(), obj.getMonto());
		return ret;
	}

	@Override
	public Datos empaquetar(Oferta obj) {
		return new Datos.Constructor().nombre(_nombreOferente)
				.apellido(_apellidoOferente).horaInicio(_horaInicio).
				horaFin(_horaFin).monto(_monto).build();
		
	}

}
