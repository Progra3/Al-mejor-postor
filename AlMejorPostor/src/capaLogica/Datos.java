package capaLogica;

import java.io.Serializable;

public class Datos {

	private final String _nombre;
	private final String _apellido;
	private final int _horaInicio;
	private final int _horaFin;
	private final int _monto;
	private final Alquiler _alquiler;
	
	public static class Constructor implements Builder<Datos>, Serializable{

		private static final long serialVersionUID = 780798160330461245L;

		private String _nombre;
		private String _apellido;
		private int _horaInicio;
		private int _horaFin;
		private int _monto;
		private Alquiler _alquiler;
		
		public Constructor nombre(String nombre){
			_nombre = nombre;
			return this;
		}
		
		public Constructor apellido(String apellido){
			_apellido = apellido;
			return this;
		}
		
		public Constructor horaInicio(int horaInicio){
			_horaInicio = horaInicio;
			return this;
		}
		
		public Constructor horaFin(int horaFin){
			_horaFin = horaFin;
			return this;
		}
		
		public Constructor monto(int monto){
			_monto = monto;
			return this;
		}

		public Constructor alquiler(int valorGuitarra, int valorBateria, int valorBajo){
			_alquiler = new Alquiler(valorGuitarra, valorBateria, valorBajo);
			return this;
		}
		
		
		@Override
		public Datos build() {
			return new Datos(this);
		}
	}
	
	public Datos(Constructor builder) {
		_nombre = builder._nombre;
		_apellido = builder._apellido;
		_horaInicio = builder._horaInicio;
		_horaFin = builder._horaFin;
		_monto = builder._monto;
		_alquiler = builder._alquiler;
	}

	public String getNombre(){return _nombre;}
	
	public String getApellido(){return _apellido;}
	
	public int getHoraInicio(){return _horaInicio;}
	
	public int getHoraFin(){return _horaFin;}
	
	public int getMonto(){return _monto;}
	
	public Alquiler getAlquiler(){return _alquiler;}

	public String toString(){
		return  _nombre + "\n" +
				_apellido + "\n" +
				_horaInicio + "\n" +
				_horaFin + "\n" +
				_monto + "\n";
	}
}
