package capaLogica;

import static org.junit.Assert.*;

import org.junit.Test;

public class OfertaTest {

	@Test
	public void equalsTest() {
		Oferta oferta = instancia();	
		Oferta oferta1 = new Oferta("Lucas", "Addisi", 0, 5, 1000);	;
		Oferta oferta2 = new Oferta("Cristian", "Canepa", 0, 5, 1000);
		
		assertNotEquals(oferta, oferta1);
		assertNotEquals(oferta, null);
		assertNotEquals(oferta, new Integer(10));
		assertEquals(oferta, oferta2);
	}

	@Test
	public void anularTest(){
		Oferta oferta = instancia();
		
		oferta.anular();
		
		assertTrue(oferta.getMonto() == 0);
		
	}
	
	@Test
	public void superPuestaConTest(){
		Oferta oferta = instancia();
		Oferta oferta1 = new Oferta("Lucas", "Addisi", 8, 21, 1000);
		Oferta oferta2 = new Oferta("Cristian", "Canepa", 0, 8, 1000);
		
		assertTrue(oferta.superpuestaCon(oferta2));
		assertFalse(oferta.superpuestaCon(oferta1));
		assertFalse(oferta1.superpuestaCon(oferta2));
	}
	
	@Test
	public void comparteToTest(){
		Oferta oferta = instancia();
		Oferta oferta1 = new Oferta("Lucas", "Addisi", 8, 21, 1000);
		Oferta oferta2 = new Oferta("Cristian", "Canepa", 0, 8, 1000);
		
		assertTrue(oferta.compareTo(oferta2) == 0);
		assertTrue(oferta.compareTo(oferta1) == -1);
		assertTrue(oferta1.compareTo(oferta2) == 1);
	}
	
	private Oferta instancia() {
		Oferta oferta = new Oferta("Cristian", "Canepa", 0, 5, 1000);
		return oferta;
	}

}
