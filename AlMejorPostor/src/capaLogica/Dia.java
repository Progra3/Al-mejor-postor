package capaLogica;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;

public class Dia {

	private Calendar _calendario;
	private Integer _fechaHoy;
	private ArrayList<Oferta> _ofertas;
	private ArrayList<Oferta> _reserva;
	
	public Dia(){
		
		_calendario = Calendar.getInstance();
		_fechaHoy = _calendario.get(Calendar.DAY_OF_MONTH);
		_ofertas = new ArrayList<Oferta>();
		_reserva = new ArrayList<Oferta>(); 
	}

	public ArrayList<Oferta> getOfertas(){
		
		return _ofertas;
	}
	
	public ArrayList<Oferta> getReserva(){
		
		return _reserva;
	}

	public void registrarOferta(String n,String a, int i, int f, int m, Alquiler al){
		if (al == null)
			al = new Alquiler(0, 0, 0);
		
		Oferta oferta = new Oferta(n, a, i, f, m);
		oferta.registrarAlquiler(al);			
		_ofertas.add(oferta);
		
	}
	
	public int montoTotal(ArrayList<Oferta> lista){
		
		int total = 0;
		for (Oferta l : lista)
			total += l.getMonto();
		
		return total;
	}

	public int ganancia(){
		
		return montoTotal(_reserva);			
	}
	
	public Oferta buscarOferta(String persona){
		
		try{
			for (Oferta o : _ofertas) if(o.getOferente() == persona)
				return o;
			
		} catch(Exception e){
			throw new IllegalArgumentException("No existe una oferta a nombre de "
														+ persona + e.getMessage());
		}
		
		return null;	
		
	}
	
	public Oferta ofertaMejorPaga(ArrayList<Oferta> lista){
		
		if (lista != null){
					
			Oferta mejor = new Oferta("", "", 0, 24, 0);
			for (Oferta o : lista){
				if (o.pagoPorHora() > mejor.pagoPorHora())
					mejor = o;
			}			
			return mejor;
		} else
			return null;
	}
	
	public String mostrarOfertas(){
		
		String ret = "Ofertas del d�a " + construirFecha() + ": \n";		
		for (Oferta oferta : _ofertas)
			ret += "\n" + oferta.toString();
		
		return ret;		
	}
	
	public String mostrarReserva(){
		
		String ret = "Reserva del d�a " + construirFecha() + ": \n";		
		for (Oferta r : _reserva)
			ret += "\n" + r.toString();
		
		return ret;		
	}
	
	public void administrarReserva(){
		
		@SuppressWarnings("unchecked")
		ArrayList<Oferta> lista = (ArrayList<Oferta>) _ofertas.clone();
		ArrayList<Oferta> temp = new ArrayList<Oferta>();
		
		ordenarPorHora(lista);
		
		while ( ! lista.isEmpty()){
			candidatasParaRangoHorario(lista, temp);
			anularSuperpuestasConUltimaReserva(lista, temp);
			vaciarListaDeAnuladas(temp);
			elegirMejorCandidata(lista, temp);
		}
	}

	private void elegirMejorCandidata(ArrayList<Oferta> lista, ArrayList<Oferta> temp) {
		if ( ! temp.isEmpty()){
			_reserva.add(ofertaMejorPaga(temp));
			for (Oferta t : temp)
				lista.remove(t);
				temp.clear();
		}
	}

	private void vaciarListaDeAnuladas(ArrayList<Oferta> temp) {
		if (montoTotal(temp) == 0)
			temp.clear();
	}

	private void anularSuperpuestasConUltimaReserva(ArrayList<Oferta> lista, ArrayList<Oferta> temp) {
		if ( ! _reserva.isEmpty())
			for (Oferta t : temp)
				if (_reserva.get(_reserva.size()-1).superpuestaCon(t)){
					lista.remove(t);	
					t.anular();
				}
	}

	private void candidatasParaRangoHorario(ArrayList<Oferta> lista, ArrayList<Oferta> temp) {
		for (Oferta o : lista){
			if (lista.get(0).superpuestaCon(o))
				temp.add(o);
		}
	}

	private void ordenarPorHora(ArrayList<Oferta> lista) {
		Collections.sort(lista);
	} 
		
	private String construirFecha(){
		_calendario = Calendar.getInstance();
		_fechaHoy = _calendario.get(Calendar.DAY_OF_MONTH);
		String ret = _fechaHoy.toString() + "/";
		ret += ((Integer) _calendario.get(Calendar.MONTH)).toString() + "/";
		ret += ((Integer) _calendario.get(Calendar.YEAR)).toString();
		
		return ret;
	}
	
}
