package capaLogica;


import static org.junit.Assert.*;

import org.junit.Test;

public class DiaTest {

	@Test
	public void testRegistrarOferta() {
		Dia dia = instancia();
		Oferta comparacion = new Oferta("Cristian","Canepa", 10, 15, 2000);
		dia.registrarOferta("Cristian","Canepa", 10, 15, 2000, new Alquiler(0, 0, 0));
		
		assertEquals(comparacion, dia.getOfertas().get(0));
		
	}

	@Test
	public void testMostrarOfertas() {
		Dia dia = instanciaConOfertas();
		System.out.println(dia.mostrarOfertas());
	}
	
	@Test
	public void testAdministrarReserva() {
		Dia dia = instanciaParaAdministrar();
		dia.administrarReserva();
		System.out.println(dia.mostrarReserva());
		System.out.println();
		System.out.println("Ganancia del d�a: " + dia.ganancia());
			
	}

	private Dia instancia() {
		Dia dia = new Dia();
		return dia;
	}

	private Dia instanciaConOfertas() {
		Dia dia = new Dia();
		for (int x = 0; x < 8; x++)
			dia.registrarOferta("Cristian","Canepa", 10 + x, 15 + x, 2000 + x * 20,
					new Alquiler(x * 300, x * 150, x * 200));
		return dia;
	}
	
	private Dia instanciaParaAdministrar(){
		Dia dia = new Dia();
		
		dia.registrarOferta("Cristian","A", 10, 11, 500, new Alquiler(0, 0, 0));
		dia.registrarOferta("Cristian","B", 10, 11, 300, new Alquiler(0, 0, 0));
		dia.registrarOferta("Cristian","C", 11, 13, 200, new Alquiler(0, 0, 0));
		dia.registrarOferta("Cristian","D", 11, 16, 400, new Alquiler(0, 0, 0));
		dia.registrarOferta("Cristian","E", 12, 15, 500, new Alquiler(0, 0, 0));
		dia.registrarOferta("Cristian","F", 13, 16, 600, new Alquiler(0, 0, 0));
		dia.registrarOferta("Cristian","G", 15, 18, 500, new Alquiler(0, 0, 0));
		dia.registrarOferta("Cristian","H", 14, 15, 100, new Alquiler(0, 0, 0));
		dia.registrarOferta("Cristian","I", 11, 15, 300, new Alquiler(0, 0, 0));
		dia.registrarOferta("Cristian","J", 18, 20, 300, new Alquiler(0, 0, 0));
		dia.registrarOferta("Cristian","K", 20, 23, 500, new Alquiler(0, 0, 0));
		dia.registrarOferta("Cristian","L", 19, 21, 250, new Alquiler(0, 0, 0));
		dia.registrarOferta("Cristian","M", 18, 20, 500, new Alquiler(0, 0, 0));
		dia.registrarOferta("Cristian","N", 17, 18, 100, new Alquiler(0, 0, 0));
		dia.registrarOferta("Cristian","O", 20, 24, 800, new Alquiler(0, 0, 0));
		
		return dia;
	}
}
