package capaLogica;

public interface Builder<E> {
	public E build();
}
