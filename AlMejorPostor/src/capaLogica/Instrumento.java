package capaLogica;

public class Instrumento {

	private String nombre;
	private boolean alquilado; //TODO más de una resposanbilidad?
	public int valor;
	
	private Instrumento(String name, int value){
		
		nombre = name;
		valor = value;
		alquilado = false;
	}
	
	
	public static Instrumento guitarra(int p){
		
		return new Instrumento("Guitarra", p);
	}
	

	public static Instrumento bajo(int p){
		
		return new Instrumento("Bajo", p);
	}	
	
	public static Instrumento bateria(int p){
		
		return new Instrumento("Bateria", p);
	}
	
	
	public String getNombre(){
		
		return nombre;
	}
	
	public void alquilar(){
		
		alquilado = true;
	}
	
	public void quitar(){
		
		alquilado = false;
	}
	
	public boolean alquilado(){
		
		return alquilado;
	}
	
	public String toString(){
		
		return nombre + ": " + valor;
	}
}
