package capaLogica;

import java.util.ArrayList;

public class Alquiler {

	private ArrayList<Instrumento> _instrumentos;
	private int _valor;
	
	public Alquiler(int guitar, int bass, int drums) {

		_instrumentos = new ArrayList<Instrumento>();
		_instrumentos.add(Instrumento.guitarra(guitar));
		_instrumentos.add(Instrumento.bajo(bass));
		_instrumentos.add(Instrumento.bateria(drums));

		_valor = 0;
	}

	public int getValor() {

		actualizarValor();
		return _valor;
	}
	
	private void actualizarValor() {

		for (Instrumento i : _instrumentos)
			if (i.alquilado())
				this._valor += i.valor;
	}

}
